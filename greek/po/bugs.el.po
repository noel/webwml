msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2006-08-12 17:38+0200\n"
"Last-Translator: George Papamichelakis\n"
"Language-Team: Greek\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/Bugs/pkgreport-opts.inc:17
msgid "in package"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:20
#: ../../english/Bugs/pkgreport-opts.inc:60
#: ../../english/Bugs/pkgreport-opts.inc:94
msgid "tagged"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:23
#: ../../english/Bugs/pkgreport-opts.inc:63
#: ../../english/Bugs/pkgreport-opts.inc:97
#, fuzzy
#| msgid "severities"
msgid "with severity"
msgstr "σοβαρότητα"

#: ../../english/Bugs/pkgreport-opts.inc:26
msgid "in source package"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:29
msgid "in packages maintained by"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:32
msgid "submitted by"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:35
msgid "owned by"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:38
#, fuzzy
#| msgid "status"
msgid "with status"
msgstr "κατάσταση"

#: ../../english/Bugs/pkgreport-opts.inc:41
msgid "with mail from"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:44
msgid "newest bugs"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:57
#: ../../english/Bugs/pkgreport-opts.inc:91
msgid "with subject containing"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:66
#: ../../english/Bugs/pkgreport-opts.inc:100
msgid "with pending state"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:69
#: ../../english/Bugs/pkgreport-opts.inc:103
msgid "with submitter containing"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:72
#: ../../english/Bugs/pkgreport-opts.inc:106
msgid "with forwarded containing"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:75
#: ../../english/Bugs/pkgreport-opts.inc:109
msgid "with owner containing"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:78
#: ../../english/Bugs/pkgreport-opts.inc:112
msgid "with package"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:122
msgid "normal"
msgstr "κανονικό"

#: ../../english/Bugs/pkgreport-opts.inc:125
msgid "oldview"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:128
msgid "raw"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:131
#, fuzzy
#| msgid "sarge"
msgid "age"
msgstr "sarge"

#: ../../english/Bugs/pkgreport-opts.inc:137
msgid "Repeat Merged"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:138
msgid "Reverse Bugs"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:139
msgid "Reverse Pending"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:140
#, fuzzy
#| msgid "Reverse order of:"
msgid "Reverse Severity"
msgstr "Ανάστροφη ταξινόμηση του:"

#: ../../english/Bugs/pkgreport-opts.inc:141
msgid "No Bugs which affect packages"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:143
msgid "None"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:144
msgid "testing"
msgstr "δοκιμαστικό"

#: ../../english/Bugs/pkgreport-opts.inc:145
msgid "oldstable"
msgstr "παλαιό σταθερό"

#: ../../english/Bugs/pkgreport-opts.inc:146
msgid "stable"
msgstr "σταθερό"

#: ../../english/Bugs/pkgreport-opts.inc:147
msgid "experimental"
msgstr "πειραματικό"

#: ../../english/Bugs/pkgreport-opts.inc:148
msgid "unstable"
msgstr "ασταθές"

#: ../../english/Bugs/pkgreport-opts.inc:152
#, fuzzy
#| msgid "archived bugs"
msgid "Unarchived"
msgstr "παλαιότερα προβλήματα"

#: ../../english/Bugs/pkgreport-opts.inc:155
#, fuzzy
#| msgid "archived bugs"
msgid "Archived"
msgstr "παλαιότερα προβλήματα"

#: ../../english/Bugs/pkgreport-opts.inc:158
msgid "Archived and Unarchived"
msgstr ""

#~ msgid "Flags:"
#~ msgstr "Σημάνσεις:"

#~ msgid "active bugs"
#~ msgstr "ενεργά προβλήματα"

#~ msgid "display merged bugs only once"
#~ msgstr "εμφάνισε τα συγχωνευμένα σφάλματα μια φορά"

#~ msgid "no ordering by status or severity"
#~ msgstr "χωρίς ταξινόμηση ανα κατάσταση ή σοβαρότητα"

#~ msgid "don't show table of contents in the header"
#~ msgstr "χωρίς εμφάνιση περιεχομένων στην κεφαλίδα"

#~ msgid "don't show statistics in the footer"
#~ msgstr "χωρίς εμφάνιση στατιστικών στο υποσέλιδο"

#~ msgid "proposed-updates"
#~ msgstr "προτεινόμενες ενημερώσεις"

#~ msgid "testing-proposed-updates"
#~ msgstr "προτεινόμενες ενημερώσεις δοκιμαστικού"

#~ msgid "Package version:"
#~ msgstr "Έκδοση πακέτου"

#~ msgid "Distribution:"
#~ msgstr "Διανομή:"

#~ msgid "bugs"
#~ msgstr "προβλήματα"

#~ msgid "open"
#~ msgstr "ανοιχτό"

#~ msgid "forwarded"
#~ msgstr "προωθημένο"

#~ msgid "pending"
#~ msgstr "εκκρεμές"

#~ msgid "fixed"
#~ msgstr "διορθωμένο"

#~ msgid "done"
#~ msgstr "έτοιμο"

#~ msgid "Include status:"
#~ msgstr "Συμπεριέλαβε κατάσταση:"

#~ msgid "Exclude status:"
#~ msgstr "Άνευ κατάστασης:"

#~ msgid "critical"
#~ msgstr "κρίσιμο"

#~ msgid "grave"
#~ msgstr "σοβαρότατο"

#~ msgid "serious"
#~ msgstr "σοβαρό"

#~ msgid "important"
#~ msgstr "σημαντικό"

#~ msgid "minor"
#~ msgstr "ασήμαντο"

#~ msgid "wishlist"
#~ msgstr "επιθυμία"

#~ msgid "Include severity:"
#~ msgstr "Συμπεριέλαβε σοβαρότητα:"

#~ msgid "Exclude severity:"
#~ msgstr "Άνευ σοβαρότητας:"

#~ msgid "potato"
#~ msgstr "potato"

#~ msgid "woody"
#~ msgstr "woody"

#~ msgid "sarge-ignore"
#~ msgstr "sarge-ignore"

#~ msgid "etch"
#~ msgstr "etch"

#~ msgid "etch-ignore"
#~ msgstr "etch-ignore"

#~ msgid "sid"
#~ msgstr "sid"

#~ msgid "confirmed"
#~ msgstr "επιβεβαιωμένο"

#~ msgid "d-i"
#~ msgstr "d-i"

#~ msgid "fixed-in-experimental"
#~ msgstr "διορθωμένο στο πειραματικό"

#~ msgid "fixed-upstream"
#~ msgstr "διορθωμένο upstream"

#~ msgid "help"
#~ msgstr "βοήθεια"

#~ msgid "l10n"
#~ msgstr "τοπικοποίηση"

#~ msgid "moreinfo"
#~ msgstr "περισσότερα"

#~ msgid "patch"
#~ msgstr "εππίραμα"

#~ msgid "security"
#~ msgstr "ασφάλεια"

#~ msgid "unreproducible"
#~ msgstr "μη επαναλαμβανόμενο"

#~ msgid "upstream"
#~ msgstr "upstream"

#~ msgid "wontfix"
#~ msgstr "χωρίς λύση"

#~ msgid "ipv6"
#~ msgstr "ipv6"

#~ msgid "lfs"
#~ msgstr "lfs"

#~ msgid "Include tag:"
#~ msgstr "Συμπεριλαμβάνει ετικέτα:"

#~ msgid "Exclude tag:"
#~ msgstr "Χωρίς ετικέτα:"
