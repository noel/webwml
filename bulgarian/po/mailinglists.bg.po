# use wml::debian::translation-check translation="1.28"
# Damyan Ivanov <dmn@debian.org>, 2011, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: 1.28\n"
"PO-Revision-Date: 2012-05-01 09:03+0300\n"
"Last-Translator: Damyan Ivanov <dmn@debian.org>\n"
"Language-Team: Български <dict@fsa-bg.org>\n"
"Language: Bulgarian\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"

#: ../../english/MailingLists/mklist.tags:6
msgid "Mailing List Subscription"
msgstr "Абониране за пощенски списък"

#: ../../english/MailingLists/mklist.tags:9
msgid ""
"See the <a href=\"./#subunsub\">mailing lists</a> page for information on "
"how to subscribe using e-mail. An <a href=\"unsubscribe\">unsubscription web "
"form</a> is also available, for unsubscribing from mailing lists. "
msgstr ""
"Прочетете страницата за <a href=\"./#subunsub\">пощенските списъци</a> за "
"информация как да се абонирате чрез e-mail. Достъпен е и <a href="
"\"unsubscribe\">формуляр за отписване</a> от пощенските списъци. "

#: ../../english/MailingLists/mklist.tags:12
msgid ""
"Note that most Debian mailing lists are public forums. Any mails sent to "
"them will be published in public mailing list archives and indexed by search "
"engines. You should only subscribe to Debian mailing lists using an e-mail "
"address that you do not mind being made public."
msgstr ""
"Имайте предвид, че повечето пощенски списъци на Дебиан са публични. Всяко "
"писмо, изпратено до тях ще бъде публикувано в общодостъпен архив и ще бъде "
"индексирано от търсещи машини. Абонирайте се за пощенските списъци на Дебиан "
"с адрес, за който нямате против да стане публично известен."

#: ../../english/MailingLists/mklist.tags:15
msgid ""
"Please select which lists you want to subscribe to (the number of "
"subscriptions is limited, if your request doesn't succeed, please use <a "
"href=\"./#subunsub\">another method</a>):"
msgstr ""
"Изберете списъците, за които желаете да се абонирате (броят абонаменти е "
"ограничен. Ако заявката не успее опитайте <a href=\"./#subunsub"
"\">алтернативния метод за абониране</a>):"

#: ../../english/MailingLists/mklist.tags:18
msgid "No description given"
msgstr "Без описание"

#: ../../english/MailingLists/mklist.tags:21
msgid "Moderated:"
msgstr "Контролиран:"

#: ../../english/MailingLists/mklist.tags:24
msgid "Posting messages allowed only to subscribers."
msgstr "Изпращането на съобщения е разрешено само за абонати."

#: ../../english/MailingLists/mklist.tags:27
msgid ""
"Only messages signed by a Debian developer will be accepted by this list."
msgstr "Приемат се само съобщения, подписани от сътрудник на Дебиан."

#: ../../english/MailingLists/mklist.tags:30
msgid "Subscription:"
msgstr "Абониране:"

#: ../../english/MailingLists/mklist.tags:33
msgid "is a read-only, digestified version."
msgstr "е само за четене, версия дайджест"

#: ../../english/MailingLists/mklist.tags:36
msgid "Your E-Mail address:"
msgstr "Вашият E-Mail адрес:"

#: ../../english/MailingLists/mklist.tags:39
msgid "Subscribe"
msgstr "Абониране"

#: ../../english/MailingLists/mklist.tags:42
msgid "Clear"
msgstr "Изчистване"

#: ../../english/MailingLists/mklist.tags:45
msgid ""
"Please respect the <a href=\"./#ads\">Debian mailing list advertising "
"policy</a>."
msgstr ""
"Съобразявайте се с <a href=\"./#ads\">правилата за рекламиране в пощенските "
"списъци на Дебиан</a>."

#: ../../english/MailingLists/mklist.tags:48
msgid "Mailing List Unsubscription"
msgstr "Отписване от пощенски списъци"

#: ../../english/MailingLists/mklist.tags:51
msgid ""
"See the <a href=\"./#subunsub\">mailing lists</a> page for information on "
"how to unsubscribe using e-mail. An <a href=\"subscribe\">subscription web "
"form</a> is also available, for subscribing to mailing lists. "
msgstr ""
"Прочетете страницата за <a href=\"./#subunsub\">пощенските списъци</a> за "
"информация как да се отпишете чрез e-mail. Достъпен е и <a href=\"subscribe"
"\">формуляр за абониране</a>. "

#: ../../english/MailingLists/mklist.tags:54
msgid ""
"Please select which lists you want to unsubscribe from (the number of "
"unsubscriptions is limited, if your request doesn't succeed, please use <a "
"href=\"./#subunsub\">another method</a>):"
msgstr ""
"Изберете списъците, от които желаете да се отпишете (броят е ограничен. Ако "
"заявката не успее опитайте <a href=\"./#subunsub\">алтернативния начин</a>):"

#: ../../english/MailingLists/mklist.tags:57
msgid "Unsubscribe"
msgstr "Отписване"

#: ../../english/MailingLists/mklist.tags:60
msgid "open"
msgstr "отворен"

#: ../../english/MailingLists/mklist.tags:63
msgid "closed"
msgstr "затворен"

#~ msgid "Please select which lists you want to unsubscribe from:"
#~ msgstr "Изберете от кои списъци желаете да се отпишете:"

#~ msgid "Please select which lists you want to subscribe to:"
#~ msgstr "Изберете за кои списъци желаете да се абонирате:"
